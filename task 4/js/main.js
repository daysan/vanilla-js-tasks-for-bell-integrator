var maxPage = 5;
var timerActive = true;
var timeLimit = 12;
var timeZone = document.getElementById('timer');

function timer(){
    if(page == maxPage || !timerActive) {
        return false;
    }
    
    timeLimit = timeLimit - 1;
    if (timeLimit == 0){
        pageTo();
    }
    timeZone.innerHTML = timeLimit;

    setTimeout(timer, 1000);
    
}
function stop() {
    timerActive = false;
}
function play(){
    if(timerActive) {
        return false;
    }
    timerActive = true;
    timer();
}
function pageTo(p) {
    if(p) {
        if(p >= 1 && p <= maxPage){
            window.location.replace(''+p+'.html');
            return false;
        }
    }
    if(page == maxPage){
        return false;
    }
    window.location.replace(''+(page + 1)+'.html');
}
function next() {
    if(page == maxPage) {
        return false;
    }
    pageTo(page + 1);
}
function prev() {
    if(page == 1) {
        return false;
    }
    var prev = page - 1; 
    console.log(prev);
    pageTo(prev);
}
function goStart(){
    pageTo(1);
}
function stopProject() {
    window.location.replace('https://google.com');
}
timer();