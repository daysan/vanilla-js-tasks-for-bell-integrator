var leftlist = document.getElementById('leftlist');
var rightlist = document.getElementById('rightlist');
var selected;
function transfer (from, to) {
  var child = from.children;
  selected = false;
  for (var i = 0; i < child.length; i++) {
    if (child[i].selected) {
      selected = true;
      while (child[i].selected) {
        var r = to.appendChild(child[i]);
        r.selected = false;
        if (!child[i]) {
          return false;
        }
      }
    }
    
  }
  if (!selected) {
    alert('Ни один элемент не выбран');
  }
}
function transferAll (from, to) {
  var child = from.children;
  if (!child.length) {
    alert('Данный список пуст!');
    return false;
  }
  for (var i = 0; i < child.length; i++) {
    while (child[i]) {
    var r = to.appendChild(child[i]);
    r.selected = false;
  }
  }
}
function sendOne() {
  transfer(leftlist, rightlist);
}
function acceptOne() {
  transfer(rightlist, leftlist);
}
function sendAll() {
    transferAll(leftlist, rightlist);
}
function acceptAll() {
    transferAll(rightlist, leftlist);
}
